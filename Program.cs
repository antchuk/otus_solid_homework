﻿using Guesser;
using System.Resources;

namespace OTUS_SOLID_HomeWork
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"SOLID homework!\nAttempt count = {Properties.Resources.AttemptCount}\n" +
                $"Range from {Properties.Resources.RangeMin} to {Properties.Resources.RangeMax}");
            int _att = int.Parse(Properties.Resources.AttemptCount);
            int _rMax = int.Parse(Properties.Resources.RangeMax);
            int _rMin = int.Parse(Properties.Resources.RangeMin);
            IntGuesser _gm= new(_att, _rMin, _rMax);
            int _nowAttempts = 0;
            while (_nowAttempts<_att)
            {
                Console.WriteLine("Input integer number:");
                if (int.TryParse(Console.ReadLine(), out int _input)) 
                {
                    _gm._inputValue = _input;
                    var _res = _gm.TryGuess();
                    Console.WriteLine($"Your result: {_res}");
                    if (_res==true) { break; }
                }
                _nowAttempts++;
            }
            Console.WriteLine($"WinNumber = {_gm._winValue}");
        }
    }
}