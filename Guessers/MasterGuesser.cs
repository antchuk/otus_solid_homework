﻿using OTUS_SOLID_HomeWork.Guessers.Abstraction;

namespace Guesser
{
    /// <summary>
    /// отгадыватель какого-то значения
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MasterGuesser<T> : IMasterGuesser<T>
    {
        /// <summary>
        /// число попыток
        /// </summary>
        public int _attemptsCount;
        /// <summary>
        /// верхняя граница диапазхона отгадывания
        /// </summary>
        public T _rangeMax;
        /// <summary>
        /// нижняя граница диапазхона отгадывания
        /// </summary>
        public T _rangeMin;
        /// <summary>
        /// целевое значение
        /// </summary>
        public T _winValue;
        /// <summary>
        /// текущее введеное значение пользователем
        /// </summary>
        public T _inputValue;
        public MasterGuesser(int Count, T Min, T Max)
        {
            _rangeMax = Max;
            _rangeMin = Min;
            _attemptsCount = Count;
        }
        /// <summary>
        /// получить введенное значение пользователем
        /// </summary>
        /// <param name="_inp"></param>
        public virtual void GetInputValue(T _inp)
        {
            _inputValue = _inp;
        }
        /// <summary>
        /// сравниваем и проверям, угадал ли пользователь
        /// </summary>
        /// <returns></returns>
        public virtual bool TryGuess()
        {            
            return false;
        }
    }
}