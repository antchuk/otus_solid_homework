﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_SOLID_HomeWork.Guessers.Abstraction
{
    public interface IMasterGuesser<T>
    {
        void GetInputValue(T input);
        bool TryGuess();
    }
}
