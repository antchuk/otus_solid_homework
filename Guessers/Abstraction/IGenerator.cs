﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_SOLID_HomeWork.Guessers.Abstraction
{
    public interface IGenerator<T>
    {
        T Generate(T MinValue, T MaxValue);
    }
}
