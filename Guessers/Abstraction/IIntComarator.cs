﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_SOLID_HomeWork.Guessers.Abstraction
{
    public interface IIntComarator
    {
        bool CompareInts(int num1, int num2);
    }
}
