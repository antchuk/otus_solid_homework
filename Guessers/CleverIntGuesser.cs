﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guesser
{
    public class CleverIntGuesser : IntGuesser
    {
        public int _attemptNow;
        public CleverIntGuesser(int _c, int _min, int _max) :base(_c, _min, _max)
        {
            if ( _c <= 0 ) 
            {
                throw new ArgumentException("Number of attempts has to be positive and largre than 0");
            }
            if ( _min < 0  || _min>_max ) 
            {
                throw new ArgumentOutOfRangeException("MIn and MAx range limits have to be positive and largre than 0. Max should be larger than Min");
            }
            _attemptNow = 0;
        }
        /// <summary>
        /// обновление настроек
        /// </summary>
        /// <param name="AttemptsCount"></param>
        /// <param name="RamgeMax"></param>
        /// <param name="RangeMin"></param>
        public void Reload(int AttemptsCount, int RamgeMax, int RangeMin)
        {
            _attemptsCount = AttemptsCount;
            _rangeMax = RamgeMax;
            _rangeMin = RangeMin;
            _attemptNow = 0;
        }

    }
}
