﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTUS_SOLID_HomeWork.Guessers.Abstraction;

namespace Guesser
{
    /// <summary>
    /// Отгадыватель числа типа int
    /// </summary>
    public class IntGuesser : MasterGuesser<int>, IGenerator<int>, IIntComarator
    {
        /// <summary>
        /// вводимое число
        /// </summary>
        public int _input;
        public IntGuesser(int _c, int _min, int _max) : base(_c, _min, _max)
        {         
            _winValue = Generate(_min,_max);
        }
        /// <summary>
        /// Генератор искомого числа
        /// </summary>
        /// <param name="_rMin"></param>
        /// <param name="_rMax"></param>
        /// <returns></returns>
        public int Generate(int _rMin, int _rMax)
        {            
            var rnd = new Random();
            return rnd.Next(_rMin, _rMax);            
        }
        /// <summary>
        /// попытка пользоватиеля отгадать
        /// </summary>
        /// <returns></returns>
        public override bool TryGuess()
        {
            return CompareInts(_inputValue, _winValue);
        }
        /// <summary>
        /// сравниваем два числа
        /// </summary>
        /// <param name="input"></param>
        /// <param name="win"></param>
        /// <returns></returns>
        public bool CompareInts(int input, int win)
        {
            return input == win;
        }
    }
}
